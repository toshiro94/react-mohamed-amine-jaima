var products = [{
    "id": 1,
    "frequencies": [
        "Just Once",
        "Every Week",
        "Every 2 Weeks",
        "Every Month",
        "Every 2 Months",
        "Every 3 Months"
    ],
    "selectedFrequency": null,
    "name": "100% Natural Spring Water",
    "size": "125 ml – Single Bottle",
    "rating": 4,
    "pricePerUnit": 5,
    "quantity": 1,
    "isNew": true,
    "imgSrc": "https://coeportal114.saphosting.de/watersdesign/images/cropped_images/5gall-254-254.png"
}, {
    "id": 2,
    "frequencies": [
        "Just Once",
        "Every Week",
        "Every 2 Weeks",
        "Every Month",
        "Every 2 Months",
        "Every 3 Months"
    ],
    "inCart": true,
    "selectedFrequency": null,
    "name": "100% Natural Spring Water",
    "size": "125 ml – Single Bottle",
    "rating": 5,
    "pricePerUnit": 5,
    "quantity": 1,
    "isNew": false,
    "imgSrc": "https://coeportal114.saphosting.de/watersdesign/images/cropped_images/perrier-254-254.png"
}, {
    "id": 3,
    "frequencies": [
        "Just Once",
        "Every Week",
        "Every 2 Weeks",
        "Every Month",
        "Every 2 Months",
        "Every 3 Months"
    ],
    "selectedFrequency": null,
    "name": "100% Natural Spring Water",
    "size": "125 ml – Single Bottle",
    "rating": 4,
    "pricePerUnit": 3,
    "quantity": 1,
    "isNew": true,
    "imgSrc": "https://coeportal114.saphosting.de/watersdesign/images/cropped_images/5gall-254-254.png"
}, {
    "id": 4,
    "frequencies": [
        "Just Once",
        "Every Week",
        "Every 2 Weeks",
        "Every Month",
        "Every 2 Months",
        "Every 3 Months"
    ],
    "selectedFrequency": null,
    "name": "100% Natural Spring Water",
    "size": "125 ml – Single Bottle",
    "rating": 2,
    "pricePerUnit": 5,
    "quantity": 1,
    "isNew": true,
    "imgSrc": "https://coeportal114.saphosting.de/watersdesign/images/Perrier-Pack-Big.png"
}, {
    "id": 5,
    "frequencies": [
        "Just Once",
        "Every Week",
        "Every 2 Weeks",
        "Every Month",
        "Every 2 Months",
        "Every 3 Months"
    ],
    "selectedFrequency": null,
    "name": "100% Natural Spring Water",
    "size": "125 ml – Single Bottle",
    "rating": 4,
    "pricePerUnit": 3,
    "quantity": 1,
    "isNew": true,
    "imgSrc": "https://coeportal114.saphosting.de/watersdesign/images/Perrier-Pack-Small.png "
}, {
    "id": 6,
    "frequencies": [
        "Just Once",
        "Every Week",
        "Every 2 Months",
        "Every 3 Months"
    ],
    "selectedFrequency": null,
    "name": "100% Natural Spring Water",
    "size": "125 ml – Single Bottle",
    "rating": 4,
    "pricePerUnit": 3,
    "quantity": 1,
    "isNew": true,
    "imgSrc": "https://coeportal114.saphosting.de/watersdesign/images/cropped_images/5gall-254-254.png"
}, {
    "id": 7,
    "frequencies": [
        "Just Once",
        "Every Week",
        "Every 2 Weeks",
        "Every Month",
        "Every 2 Months",
        "Every 3 Months"
    ],
    "selectedFrequency": "Every Month",
    "name": "100% Natural Spring Water",
    "size": "125 ml – Single Bottle",
    "rating": 4,
    "pricePerUnit": 3,
    "quantity": 1,
    "isNew": true,
    "imgSrc": "https://coeportal114.saphosting.de/watersdesign/images/Perrier-Pack-Big.png"
}];

export {products};