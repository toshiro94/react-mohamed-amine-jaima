import React, { Component } from 'react';
class Description extends Component {
	render() {
		return (
			<section className="description">
         		<div className="main">
              		<div className="description-menu">
                		Ready  - Water - Sparkling Water Lime
             		</div>
              		<div className="description-elements">
	                 	<h1 className="description-elements__title">
	                    All Products
	                  	</h1>
	                  	<button className="description-elements__sort">
	                    Sort by / Filter
	                 	</button>
              		</div>
          		</div>
      		</section>
		);
	}
}

export {Description};