module.exports = function(grunt) {
    grunt.initConfig({
        cssmin: {
            target: {
                files: [{
                    expand: true,
                    cwd: 'src/css',
                    src: ['*.css', '!*.min.css'],
                    dest: 'src/css',
                    ext: '.min.css'
                }]
            }
        },
        sass: {
            options: {
                sourceMap: true
            },
            dist: {
                files: {
                    'src/css/style.css': 'src/scss/main.scss'
                }
            }
        },
        cssbeautifier: {
            files: ["src/css/style.css"],
            options: {
                indent: '  ',
                openbrace: 'end-of-line',
                autosemicolon: false
            }
        }
    });


    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-cssbeautifier');
    grunt.loadNpmTasks('grunt-serve');

    grunt.registerTask('build', ['sass', 'cssbeautifier', 'cssmin']);

};