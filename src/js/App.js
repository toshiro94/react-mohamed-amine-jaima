import React, { Component } from 'react';
import {Description} from './Description';
import {Header} from './Header';
import {PromotionBanner} from './PromotionBanner';
import {Products} from './Products';


class App extends Component {

  constructor(props) {
    super(props);

    this.updateCart = this.updateCart.bind(this);
    this.showCart = this.showCart.bind(this);
    this.showBody = this.showBody.bind(this);

    this.state = {
      cart: [],
      change: ''
    }
  }

  showBody(e) { 
    if(!document.querySelector('.cart-table').contains(e.target)) {
      document.querySelector('.fadein').className = 'fadein';
      document.querySelector('.cart-table').className = 'cart-table hidden';
    }
  }

  showCart() {
    if (document.querySelector('.fadein').className.indexOf('darken') !== -1) {
      document.querySelector('.fadein').className = 'fadein';
      document.querySelector('.cart-table').className = 'cart-table hidden';
    } else {
      document.querySelector('.fadein').className += ' darken';
      document.querySelector('.cart-table').className = 'cart-table';
    }
  }


  updateCart(product, input) {
    let el = {product, input};
    this.state.cart.push(el);
    console.log(this.state.cart);
    this.setState({
      change: ''
    });
  }

  render() {

    return (
      <div className="page-container" onClickCapture={this.showBody}>
      
        <Header cart={this.state.cart} showCart={this.showCart}  />
        <Description />
        <PromotionBanner />
        <Products products={this.props.products} updateCart={this.updateCart}/>
        <div className="fadein"></div>

    </div>
    );
  }
}


export default App;