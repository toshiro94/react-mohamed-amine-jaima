import React, { Component } from 'react';

class ProductRow extends Component {
  constructor(props) {
    super(props);

    this.state = {
      frequency: '',
      quantity: 1
    }

    this.addQte = this.addQte.bind(this);
    this.removeQte = this.removeQte.bind(this);
    this.selectFrequency = this.selectFrequency.bind(this);
    this.addToCart = this.addToCart.bind(this);
  }

  addToCart() {
    this.props.updateCart(this.props.product, this.state);
  }

  addQte() {
    this.setState({
      quantity: this.state.quantity + 1
    });
  }

  removeQte() {
    if (this.state.quantity > 1) {
      this.setState({
      quantity: this.state.quantity - 1
      });
    }
  }

  selectFrequency(e) {
    this.setState({
      frequency: e.target.value
    });
  }

  render() {
    const stars = [];
    const rating = this.props.product.rating;
    const frequencies = [];

    for (let i = 0; i< rating; i++) {
      stars.push(<span key={i} className="star">&#9733;</span>);
    }
    for (let i = 0; i< this.props.product.frequencies.length; i++) {
      frequencies.push(<option key={i} value={this.props.product.frequencies[i]}>{this.props.product.frequencies[i]}</option>);
    }

    return <li>
              { this.props.product.isNew ? <span className="new-product">New</span> : ""}
              <img alt="" src={this.props.product.imgSrc} width="100%"/>
              <div className="product-details">
                <p className="product-name"><b>{this.props.product.name}</b></p>
                <p className="product-size">{this.props.product.size}</p>
                <p className="product-rating">{stars}</p>
                <p className="product-price"><b><sup>$</sup>{this.props.product.pricePerUnit}</b></p>
              </div>
              <div className="hovered">
                <select onChange={this.selectFrequency}>
                  <option value="Select a frequency">
                    Select a frequency
                  </option>
                  {frequencies}
                </select>
                <button type="button" className="product-minus" onClick={this.removeQte}>-</button>
                <span className="product-qte">{this.state.quantity}</span>
                <button type="button" className="product-plus" onClick={this.addQte}>+</button>
                <button type="button" className="product-add" onClick={this.addToCart}>Add</button>
              </div>
            </li>;
  }
}

class Products extends Component {

  constructor(props) {
    super(props);   
  }

  render() {
    const products = this.props.products;

    const output = products.map((product) => {
      return <ProductRow key={product.id} product={product} updateCart={this.props.updateCart}/>;
    });

    return (
       <section className="products">
          <div className="main" >
            <ul>
              {output}
            </ul>
          </div>
        </section>
    );
  }
}

export {Products};