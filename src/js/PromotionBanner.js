import bottle from '../img/banner-bottles.png';
import React, { Component } from 'react';
class PromotionBanner extends Component {
	render() {
		return (
				<section className="promotion-banner">
		          <div className="main">
		              <img className="image" alt="Bottle" src={bottle} />
		              <div className="desc">
		                  <h3>5-Gallon Quick Shop</h3>
		                  <span>3 easy steps to get crisp refreshing water.</span>
		              </div>
		              <button className="buy">Buy Now</button>
		          </div>
		        </section>
		);
	}
}

export {PromotionBanner};