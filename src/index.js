import './lib/font-awesome-4.7.0/css/font-awesome.css';
import './css/style.css';
import './img/logo.png';

import React from 'react';
import ReactDOM from 'react-dom';
import App from './js/App';
import {products} from './js/data';
import registerServiceWorker from './js/registerServiceWorker';

// fetch('https://demo8496746.mockable.io/products').then(function(response) {
//   return response.json();
// }).then(function(data) {

//   console.log(data);

//   ReactDOM.render(
//   <App products={data.products}/>,
//   document.getElementById('root')
// );

// registerServiceWorker();

// }).catch(function() {
//   console.log("Request Error!");
// });

//console.log(products);
 
ReactDOM.render(
  <App products={products}/>,
  document.getElementById('root')
);

registerServiceWorker();
