import React, { Component } from 'react';
import logo from '../img/logo.png';

class Header extends Component {
  constructor(props) {
    super(props);
    this.showCart = this.showCart.bind(this);
  }

  showCart() {
    this.props.showCart();
  }

	render() {

    const cart = this.props.cart.map((product, index) => {
      return  <tr key={index}>
                <td><img src={product.product.imgSrc} /></td>
                <td> 
                  <p className="cart-name">{product.product.name}</p><br /><br />
                  <p className="cart-size">{product.product.size}</p>
                  <p className="cart-frequency">{product.input.frequency}</p>
                </td>
                <td> <b>{product.input.quantity}</b></td>
                <td> <b>${product.product.pricePerUnit}</b></td>
              </tr>;
    });

    let total = 0; 

    this.props.cart.forEach((p) => {
      total += p.product.pricePerUnit * parseInt(p.input.quantity);
    });


		return (
		<header>
          <div className="sub-header">
              <div className="sub-header__left">
                  <p>Have questions or want to order? Give us a call <span>1-866-813-49391-866-813-4939</span></p>
                  <a href="#">Invite Friends & Get $10!</a>
              </div>
              <div className="sub-header__right">
                  <ul>
                      <li>Francisco Bordim
                      </li>
                      <li>
                          Next Delivery: Wednesday, June 8
                      </li>
                  </ul>
              </div>
          </div>
          <div className="main">
              <div className="main-header">
                <a href="#" className="logo-header">
                  <img src={logo} alt="Nestlé Logo" />
                </a>
                  <div className="menu-header">
                      <ul>
                          <li><a href="#">Categories <span className="fa fa-angle-down"></span></a></li>
                          <li><a href="#">Brands <span className="fa fa-angle-down"></span></a></li>
                          <li><a href="#">5-Gallon Quick Shop <span className="fa fa-angle-down"></span></a></li>
                          <li><a href="#">My Deliveries <span className="fa fa-angle-down"></span></a></li>
                      </ul>
                  </div>
                  <div className="right-header">
                      <a className="search" href="#">Search <span className="fa fa-search"></span></a>
                      <a className="cart" href="#" onClick={this.showCart}><span className="fa fa-shopping-cart"></span><span className="nbr-products">{this.props.cart.length}</span></a>
                      <div className="cart-table hidden">
                        <table>
                          <thead>
                            <tr>
                              <th>PRODUCTS</th>
                              <th></th>
                              <th>QTE</th>
                              <th>PRICE</th>
                            </tr>
                          </thead>
                          <tbody>
                            {cart}
                          </tbody>
                        </table>

                        <span className="total-title">SUBTOTAL</span>
                        <span className="total-value">${total}</span>

                        <a href="#" className="proceed">
                          <button type="button" >Proceed To Cart</button>
                        </a>
                      </div>
                  </div>
              </div>
          </div>
      </header>
		);
	}
}

export {Header};